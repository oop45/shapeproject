/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.shapeproject;

/**
 *
 * @author ASUS
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(2);
        System.out.println("Area of square1 (a = "+ square1.getA()+") is " + square1.calArea());
        square1.setA(4);//square1.a = 4;
        System.out.println("Area of square1 (a = "+ square1.getA()+") is " + square1.calArea());
        square1.setA(0);//square1.a = 0;
        System.out.println("Area of square1 (a = "+ square1.getA()+") is " + square1.calArea());
        System.out.println(square1.toString());
        System.out.println(square1);
    }
}
